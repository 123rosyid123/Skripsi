# Generated by Django 2.0.2 on 2018-05-22 14:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aktor', '0004_auto_20180519_0302'),
    ]

    operations = [
        migrations.AddField(
            model_name='mahasiswa',
            name='nama',
            field=models.CharField(default='terserah', max_length=100),
            preserve_default=False,
        ),
    ]
