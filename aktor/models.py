from django.db import models
from django.contrib.auth.models import User
from prodi.models import Prodi

# Create your models here.

PILIHAN = (
    ( 0 , 'Kaprodi'),
    ( 1, 'Dekan'),
    ( 2, 'Wakil Rektor 1'),

)

class Mahasiswa(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nim = models.CharField(max_length=20)
    nama = models.CharField(max_length=100)
    prodi = models.ForeignKey(Prodi, on_delete=models.CASCADE)
    ipk = models.FloatField(null=True, blank=True)
    ips = models.FloatField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "Mahasiswa"
        verbose_name = "Mahasiswa"
        unique_together= ("user","nim")
    def __str__(self):
        return "{}" .format(self.user)


class Dosen(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nidn = models.CharField(max_length=10)
    nik = models.CharField(max_length=10)
    prodi = models.ForeignKey(Prodi,on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Dosen"
        verbose_name = "Dosen"
        unique_together= ("user","nidn","nik")
    def __str__(self):
        return "{}" .format(self.user)

# class Struktural(models.Model):
#     user = models.ForeignKey(User, on_delete = models.CASCADE)
#     jabatan = models.PositiveSmallIntegerField(choices=PILIHAN, default=0)