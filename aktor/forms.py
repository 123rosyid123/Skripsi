from django import forms
from .models import Mahasiswa, Dosen

class MahasiswaForm(forms.ModelForm):
    class Meta:
        model = Mahasiswa
        fields = ['user', 'nim', 'prodi']

class DosenForm(forms.ModelForm):
    class Meta:
        model = Dosen
        fields = ['user', 'nidn', 'nik', 'prodi']