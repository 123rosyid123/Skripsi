from django.shortcuts import render, redirect
from .forms import MahasiswaForm, DosenForm
# Create your views here.
from .models import Mahasiswa, Dosen

def home(request):
    mahasiswa = Mahasiswa.objects.all()
    dosen = Dosen.objects.all()
    print(dosen)
    context = {'mahasiswa' : mahasiswa,
               'dosen' : dosen
    }

    return render(request, 'aktor.html', context)

#===================== Mahasiswa ==================================
def home_mahasiswa(request):
    mahasiswa = Mahasiswa.objects.all()
    context = {
        'mahasiswa' : mahasiswa
    }
    return render(request,'Mahasiswa/home_mahasiswa.html', context)

def tambah_mahasiswa(request):
    form = MahasiswaForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('home_mahasiswa_view')

    return render(request, 'Mahasiswa/mahasiswa_form.html', {'form':form})

def edit_mahasiswa(request, id):
    aktor = Mahasiswa.objects.get(id = id)
    form = MahasiswaForm(request.POST or None, instance = aktor)

    if form.is_valid():
        form.save()
        return redirect('home_mahasiswa_view')

    return render(request, 'Mahasiswa/mahasiswa_form.html', {'form':form, 'aktor':aktor})

def hapus_mahasiswa(request,id):
    aktor = Mahasiswa.objects.get(id=id)

    if  request.method=="POST":
        aktor.delete()
        return redirect('home_mahasiswa_view')

    return render(request, 'Mahasiswa/mahasiswa_delete.html', {'aktor' : aktor})

#============================= Dosen ==============================
def home_dosen(request):
    dosen = Dosen.objects.all()
    context = {
        'dosen' : dosen
    }
    return render(request,'Dosen/home_dosen.html', context)

def tambah_dosen(request):
    form = DosenForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('home_dosen_view')

    return render(request, 'Dosen/dosen_form.html', {'form':form})

def edit_dosen(request, id):
    aktor = Dosen.objects.get(id = id)
    form = DosenForm(request.POST or None, instance = aktor)

    if form.is_valid():
        form.save()
        return redirect('home_dosen_view')

    return render(request, 'Dosen/dosen_form.html', {'form':form, 'aktor':aktor})

def hapus_dosen(request,id):
    aktor = Dosen.objects.get(id=id)

    if  request.method=="POST":
        aktor.delete()
        return redirect('home_mahasiswa_view')

    return render(request, 'Dosen/dosen_delete.html', {'aktor' : aktor})