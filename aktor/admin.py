from django.contrib import admin
from .models import Dosen, Mahasiswa
# Register your models here.


class AdminMahasiswa(admin.ModelAdmin):
    list_display = ('user', 'nim', 'prodi')

class AdminDosen(admin.ModelAdmin):
    list_display = ('user', 'nik', 'prodi')

admin.site.register(Dosen, AdminDosen)
admin.site.register(Mahasiswa, AdminMahasiswa)
