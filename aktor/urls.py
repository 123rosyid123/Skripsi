from django.urls import path, include
from .views import *

# app_name='aktor'
urlpatterns = [
    path('', home, name="home_aktor_view"),
    path('mahasiswa/', include([
        path('',home_mahasiswa, name='home_mahasiswa_view'),
        path('tambah', tambah_mahasiswa , name="tambah_mahasiswa"),
        path('edit/<int:id>/', edit_mahasiswa, name="edit_mahasiswa"),
        path('delete/<int:id>/', hapus_mahasiswa, name="hapus_mahasiswa"),
    ])),
    path('dosen/',include([
        path('', home_dosen, name='home_dosen_view'),
        path('tambah', tambah_dosen, name="tambah_dosen"),
        path('edit/<int:id>/', edit_dosen, name="edit_dosen"),
        path('delete/<int:id>/', hapus_dosen, name="hapus_dosen"),
    ]))
#     path('tambah/', tambah_aktor , name="tambah_aktor"),
#     path('edit/<int:id>/', edit_aktor, name="edit_aktor"),
#     path('delete/<int:id>/', hapus_aktor, name="hapus_aktor"),
]

