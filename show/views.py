from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Sum
from django.http import HttpResponse

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.views import logout
from django.core import serializers
from django.core.serializers import serialize


from aktor.models import Dosen, Mahasiswa
from jadwal.forms import KelasForm , JamDosenForm
from jadwal.models import Kelas, Periode, JamKuliah, JamDosen
from krs.forms import KrsForm
from krs.models import Krs, Sks_ips
from matkul.forms import MatkulForm
from matkul.models import Matkul
from django.contrib.auth.models import User

import random
import json
###### decorator yang dibuat#########
from prodi.models import Ruang


def cek_kaprodi(request):
	# print(request.groups.filter(name='Kaprodi').exists())
	return request.groups.filter(name='Kaprodi').exists()


def cek_mhs(request):
	try:
		if request.mahasiswa:
			return True
	except Exception as Error:
		print(Error)
		return False


def cek_dosen(request):
	try:
		if request.dosen:
			return True
	except Exception as Error:
		print(Error)
		return False


#################################

################ validasi global ################
def cek_sks(sks_mhs, sks_now, sks_matkul):
	if sks_now == None:
		return True
	elif sks_now + sks_matkul <= sks_mhs:
		print(sks_now + sks_matkul)
		return True
	else:
		print(sks_now + sks_matkul)
		return False


###################################


@login_required(login_url='/')
def home(request):
	return render(request, "beranda.html")


def tampilan_admin(request):
	template_name = 'admin_home.html'
	contex = {}
	return render(request, template_name, contex)


@user_passes_test(cek_kaprodi, login_url='/')
def reset_matkul(request):
	periode = Periode.objects.all().order_by('id').reverse()[0]
	semester = [2, 4, 6, 8, 10]  # sebagai validasi yang diambil semester genap
	if periode.semester == 2:
		Matkul.objects.filter(prodi=request.user.dosen.prodi, semester__in=semester).update(is_aktif=True)
		Matkul.objects.filter(prodi=request.user.dosen.prodi).exclude(semester__in=semester).update(is_aktif=False)
		Matkul.objects.filter(prodi=request.user.dosen.prodi, semester=11).update(is_aktif=True)
	else:
		Matkul.objects.filter(prodi=request.user.dosen.prodi, semester__in=semester).update(is_aktif=False)
		Matkul.objects.filter(prodi=request.user.dosen.prodi).exclude(semester__in=semester).update(is_aktif=True)
		Matkul.objects.filter(prodi=request.user.dosen.prodi, semester=11).update(is_aktif=True)
	return redirect('dosen_kelola_matkul')


def login(request):
	if request.user.is_authenticated:
		return redirect('home_page')

	template_name = 'login.html'
	context = {}

	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		print(username)
		print(password)
		user = authenticate(username=username, password=password)

		if user is not None:
			auth_login(request, user)
			return redirect('home_page')
		else:
			context.update({'error_message': 'User tidak ada!'})
			return render(request, template_name, context)
	return render(request, template_name, context)


def logout(request):
	auth_logout(request)
	return redirect('login_page')

####################### view untuk dosen dan kaprodi ###############################
def tampilan_dosen(request):
	pass
	# return render(request,)


@user_passes_test(cek_kaprodi, login_url='/')
def dosen_manajemen_kelas(request):
	# kelas = Kelas.objects.all()
	# print(request.groups.filter(name='Kaprodi').exists())
	periode = Periode.objects.latest('id')
	template_name = 'kaprodi/manajemen_kelas/manajemen_kelas.html'

	def get_kelas_by_hari(hari):
		return Kelas.objects.filter(jam_kuliah__hari=hari, matkul__prodi=request.user.dosen.prodi, periode=periode)

	context = {
		'kelas_hari_senin': get_kelas_by_hari(2),
		'kelas_hari_selasa': get_kelas_by_hari(3),
		'kelas_hari_rabu': get_kelas_by_hari(4),
		'kelas_hari_kamis': get_kelas_by_hari(5),
		'kelas_hari_jumat': get_kelas_by_hari(6),
		'kelas_hari_sabtu': get_kelas_by_hari(7)
	}

	return render(request, template_name, context)


@user_passes_test(cek_kaprodi, login_url='/')
def dosen_tambah_kelas(request):
	template_name = 'kaprodi/manajemen_kelas/kelas_form.html'
	dosen_list = Dosen.objects.filter(prodi=request.user.dosen.prodi)
	matkul_list = Matkul.objects.filter(prodi=request.user.dosen.prodi, is_aktif=1)
	try:  # ini logika apakah matakuliah pada kelas tertentu apakah pernah dibuat
		if request.user.groups.filter(name='Kaprodi').exists():
			periode = Periode.objects.all().order_by('id').reverse()[0]
			jam = JamKuliah.objects.order_by('?')

			# print(jam)

			if request.method == 'POST':
				ruang = Ruang.objects.filter(kapasitas__gte=request.POST.get('kapasitas')).order_by('?')
				kelas = Kelas.objects.filter(periode=periode,matkul__prodi=request.user.dosen.prodi)
				jamdosen = JamDosen.objects.all().values_list('dosen__user', flat=True)
				# if 
				cek= JamDosen.objects.filter(periode=periode, dosen = request.POST.get('dosen'))
				# print(cek[0].jam_kuliah.all())
				cekset = set(str(x) for x in jamdosen)
				# print(request.POST.get('dosen'))
				print(cekset)
				cekdosen = set(request.POST.get('dosen'))
				print(cekdosen)
				if cekdosen & cekset:
					print(cek[0].jam_kuliah.all())

					jam = cek[0].jam_kuliah.all()
				jam_banned = set()
				for k in kelas:
					jam_banned = jam_banned | k.jam()
				print(jam_banned)
				form = KelasForm(request.POST)
				form.fields['dosen'].queryset = dosen_list
				form.fields['matkul'].queryset = matkul_list

				if form.is_valid():
					# ini untuk logika validasi dosen tidak boleh mengajar pada waktu bersamaan
					dosen = request.POST.getlist('dosen')
					matkul = Matkul.objects.get(id=request.POST.get('matkul'))
					sks_matkul = matkul.sks  # mencari jumlah sks makul yang dipilih

					for x in jam:
						# print(x)
						if Kelas.objects.filter(dosen__id__in=dosen, jam_kuliah=x, periode=periode).exists():
							continue
						else:
							# ini logika agar ruang tidak dapat dipadkai dalam waktu yang bersamaan
							for y in ruang:
								if Kelas.objects.filter(jam_kuliah=x, ruang=y, periode=periode).exists():
									continue
								else:
									kls = Kelas(jam_kuliah=x, ruang=y, periode=periode)
									form = KelasForm(request.POST, instance=kls)
									# print(form.jam())

									if form.is_valid():
										instance = form.save(commit=False)
										instance.nama = str(request.POST.get('nama')).upper()
										# print(type(instance.jam()))
										if jam_banned & instance.jam():
											print(instance.jam_kuliah)
											# print('a')
											continue
										else:
											print(instance.jam_kuliah)
											instance.save()
											form.save()
											return redirect('dosen_manajemen_kelas')
							# context = {'error_message': 'Semua kelas telah penuh !!!!!!', 'form': form}
							# return render(request, template_name, context)
					form.fields['dosen'].queryset = dosen_list
					form.fields['matkul'].queryset = matkul_list
					context = {'error_message': 'Jam dosen ini sudah penuh / tidak ada ruang yang tersedia', 'form': form}
					return render(request, template_name, context)
				else:

					context = {'form_errors': form.errors, 'form': form}
					return render(request, template_name, context)
			else:
				form = KelasForm()
				form.fields['dosen'].queryset = dosen_list
				form.fields['matkul'].queryset = matkul_list

			return render(request, template_name, {'form': form})
		else:
			return redirect('dosen_manajemen_kelas')
	except Exception as eror:
		print(eror)
		form = KelasForm(request.POST)
		form.fields['dosen'].queryset = dosen_list
		form.fields['matkul'].queryset = matkul_list
		context = {'error_message': 'Kelas Sudah tersedia', 'form': form}
		return render(request, template_name, context)


@user_passes_test(cek_kaprodi, login_url='/')
def dosen_edit_kelas(request, id):
	kelas = Kelas.objects.get(id=id)
	form = KelasForm(request.POST or None, instance=kelas)

	if form.is_valid():
		form.save()
		return redirect('home_kelas_view')

	return render(request, 'kelas_form.html', {'form': form, 'kelas': kelas})


@user_passes_test(cek_kaprodi, login_url='/')
def dosen_hapus_kelas(request, id):
	kelas = Kelas.objects.get(id=id)

	if request.method == "POST":
		kelas.delete()
		return redirect('home_kelas_view')

	return render(request, 'aktor_delete.html', {'kelas': kelas})


@user_passes_test(cek_kaprodi, login_url='/')
def dosen_manajemen_matkul(request):
	matkul = Matkul.objects.filter(is_aktif=True).filter(prodi=request.user.dosen.prodi)
	return render(request, 'kaprodi/manajemen_matkul/manajemen_matkul.html', {'matkul': matkul})

@user_passes_test(cek_kaprodi, login_url='/')
def dosen_kelola_matkul(request):
	if request.method == 'POST':
		id_list = request.POST.getlist('checks')
		Matkul.objects.filter(id__in=id_list).update(is_aktif=True)
		Matkul.objects.filter(prodi=request.user.dosen.prodi).exclude(id__in=id_list).update(is_aktif=False)

	matkul = Matkul.objects.filter(prodi=request.user.dosen.prodi).order_by('-id')
	return render(request, 'kaprodi/manajemen_matkul/kelola_matkul.html', {'matkul': matkul})


@user_passes_test(cek_kaprodi, login_url='/')
def dosen_tambah_matkul(request):
	try:
		# print(request.user.groups.filter(name='Kaprodi').exists())
		if request.user.groups.filter(name='Kaprodi').exists():
			prodi_dosen = request.user.dosen.prodi
			mk = Matkul(prodi=prodi_dosen)
			# print('asdna')
			if request.method == 'POST':
				form = MatkulForm(request.POST, instance=mk)
				if form.is_valid():
					# print(request.POST.getlist('kode'))
					form.save()
					return redirect('dosen_kelola_matkul')
			else:
				form = MatkulForm(instance=mk)

			return render(request, 'kaprodi/manajemen_matkul/matkul_form.html', {'form': form})
		else:
			return redirect('dosen_manajemen_matkul')
	except Exception as eror:
		print(eror)
		return redirect('dosen_manajemen_matkul')
	# matkul = Matkul.objects.filter(prodi=request.user.dosen.prodi)
	# return render(request, 'kaprodi/manajemen_matkul/kelola_matkul.html', {'matkul': matkul})


@user_passes_test(cek_kaprodi, login_url='/')
def dosen_edit_matkul(request, id):
	try:
		print('asda')
		if request.user.groups.filter(name='Kaprodi').exists():
			matkul = Matkul.objects.get(id=id)
			form = MatkulForm(request.POST or None, instance=matkul)

			if form.is_valid():
				form.save()
				return redirect('dosen_kelola_matkul')

			return render(request, 'kaprodi/manajemen_matkul/matkul_form.html', {'form': form, 'matkul' : matkul})
		else:
			return redirect('dosen_manajemen_matkul')
	except Exception as eror:
		print(eror)
		return redirect('dosen_manajemen_matkul')


@user_passes_test(cek_kaprodi, login_url='/')
def dosen_hapus_matkul(request):
	pass

@user_passes_test(cek_kaprodi, login_url='/')
def	dosen_manajemen_dosen(request):
	template_name = 'kaprodi/manajemen_dosen/manajemen_dosen.html'
	periode = Periode.objects.all().order_by('id').reverse()[0]
	jamdos = JamDosen.objects.filter(periode=periode, dosen__prodi=request.user.dosen.prodi)
	context = {'jamdos':jamdos}
	# asd = serialize('json', jamdos)
	# return HttpResponse(asd)
	return render(request, template_name, context)
	

@user_passes_test(cek_kaprodi, login_url='/')
def dosen_tambah_dosen(request):
	template_name = 'kaprodi/manajemen_dosen/tambah_dosen.html'
	periode = Periode.objects.all().order_by('id').reverse()[0]
	form = JamDosenForm()
	dosen_list = Dosen.objects.filter(prodi=request.user.dosen.prodi)
	print(dosen_list)
	form.fields['dosen'].queryset = dosen_list
	
	if request.method == 'POST':
		try:
			ins = JamDosen(periode=periode)	
			JamDos = JamDosenForm(request.POST, instance=ins)
			if JamDos.is_valid():
				JamDos.save()
				context = {'form':form,
							'message_success':'Jam Dosen telah ditambahkan'}
				return render(request, template_name, context)
		except Exception as eror:
			context = {'form':form,
						'message_failed':'Dosen pernah ditambahkan!!'}
			return render(request, template_name, context)
			print(eror)
		# else:
		# 	pass
		# finally:
		# 	pass
		

	context = {'form':form}
	return render(request, template_name, context)

@user_passes_test(cek_kaprodi, login_url='/')
def dosen_kelola_dosen(request):
	template_name = 'kaprodi/manajemen_dosen/kelola_dosen.html'
	periode = Periode.objects.all().order_by('id').reverse()[0]
	jamdos = JamDosen.objects.filter(periode=periode, dosen__prodi=request.user.dosen.prodi)
	context = {'jamdos':jamdos}
	return render(request, template_name, context)
@user_passes_test(cek_dosen, login_url='/')

@user_passes_test(cek_kaprodi, login_url='/')
def dosen_edit_dosen(request, id):
	jamdos = JamDosen.objects.get(id=id)
	if	jamdos.dosen.prodi == request.user.dosen.prodi:
		form = JamDosenForm(request.POST or None, instance=jamdos)

		if form.is_valid():
			form.save()
			return redirect('dosen_manajemen_dosen')

		return render(request, 'kaprodi/manajemen_dosen/tambah_dosen.html', {'form': form, 'jamdos': jamdos})
	else:
		return redirect('home_page')

def dosen_jadwal_mengajar(request):
	periode = Periode.objects.all().order_by('id').reverse()[0]

	def get_jadwal_by_hari(hari):
		return Kelas.objects.filter(jam_kuliah__hari=hari, dosen=request.user.dosen, periode=periode).order_by(
			'jam_kuliah__slot')

	template_name = 'dosen/jadwal_mengajar.html'
	context = {'judul': 'Jadwal Mengajar Dosen',
			   'jadwal_hari_senin': get_jadwal_by_hari(2),
			   'jadwal_hari_selasa': get_jadwal_by_hari(3),
			   'jadwal_hari_rabu': get_jadwal_by_hari(4),
			   'jadwal_hari_kamis': get_jadwal_by_hari(5),
			   'jadwal_hari_jumat': get_jadwal_by_hari(6),
			   'jadwal_hari_sabtu': get_jadwal_by_hari(7)

			   }
	return render(request, template_name, context)


@user_passes_test(cek_dosen, login_url='/')
def dosen_daftar_mahasiswa(request):
	# pass
	return render(request, 'dosen/daftar_mahasiswa.html')


@user_passes_test(cek_dosen, login_url='/')
def dosen_get_detail_kelas(request, id):
	periode = Periode.objects.all().order_by('id').reverse()[0]
	# ids = Krs.objects.values_list('column_name', flat=True).filter(...)
	# my_models = MyModel.objects.filter(pk__in=set(ids))
	mhs_kelas = Krs.objects.values_list('mhs', flat=True).filter(kelas=id, kelas__periode=periode)
	detailMahasiswa = Mahasiswa.objects.filter(pk__in=set(mhs_kelas))
	# detailNamamhs = User.objects.filter(pk__in=set(Mahasiswa.objects.values_list('user', flat=True).filter(pk__in=set(mhs_kelas))))

	# print(detailMahasiswa)
	data = serializers.serialize("json", detailMahasiswa)

	if len(data) <= 2:
		result = {
			'success': False,
			'message': 'data tidak ditemukan'
		}
		data = json.dumps(result)
		return HttpResponse(data, content_type="application/json", status=404)

	# print(data)
	# result = [
	#     {
	#         'success': True,
	#         'result': {
	#             'data' : data
	#         }
	#     }
	# ]

	return HttpResponse(data, content_type="application/json")


######################### view untuk tampilan mahasiswa ###################################
@user_passes_test(cek_mhs, login_url='/')
def tampilan_mahasiswa(request):
	mhs = Mahasiswa.objects.get(user=request.user)
	sks_mhs = Sks_ips.objects.filter(ips_minimum__lte=mhs.ips).order_by('-ips_minimum')[
		0]  ##untuk sks maksimum yang dapat diambil
	periode = Periode.objects.all().order_by('id').reverse()[0]
	asdasd = Kelas.objects.first().krs_set.all().count()
	print(asdasd)
	krs = Krs.objects.filter(mhs=request.user.mahasiswa, periode=periode)
	total_sks = krs.aggregate(Sum('kelas__matkul__sks'))['kelas__matkul__sks__sum']

	template_name = 'mahasiswa/manajemen_krs.html'
	context = {'krs': krs,
			   'total_sks': total_sks,
			   'sks_mhs': sks_mhs.sks}
	return render(request, template_name, context)


@user_passes_test(cek_mhs, login_url='/')
def mahasiswa_tambah_krs(request):
	mhs = Mahasiswa.objects.get(user=request.user)
	sks = Sks_ips.objects.filter(ips_minimum__lte=mhs.ips).order_by('-ips_minimum')[0]
	sks_mhs = sks.sks
	# print(sks_mhs)
	periode = Periode.objects.all().order_by('id').reverse()[0]
	print(periode)
	# kelas_penuh = Kelas.objects.filter(krs_)

	krs = Krs.objects.filter(mhs=request.user.mahasiswa, periode=periode)
	total_sks = krs.aggregate(Sum('kelas__matkul__sks'))['kelas__matkul__sks__sum']

	jam_banned = krs.values_list('kelas__jam_kuliah__slot',
								 flat=True)  # agar menampilkan makul uyang jamnya belum dipilih mhs
	matkul_banned = krs.values_list('kelas__matkul',
									flat=True)  # agar tidak bisa memilih matkul yang sama pada kelas yang berbeda
	# matkul_banned2 =    #jika kapasitas telah penuh
	kelas = Kelas.objects.filter(matkul__prodi=request.user.mahasiswa.prodi, periode=periode).exclude(jam_kuliah__in=jam_banned).exclude(
		matkul__in=matkul_banned).exclude()

	form = KrsForm()
	krs_inst = Krs(periode=periode, mhs=request.user.mahasiswa)
	# print(request.user.mahasiswa)
	if request.method == "POST":
		print(request.POST.get('kelas'))

		form = KrsForm(request.POST, instance=krs_inst)
		if form.is_valid():
			asd = Kelas.objects.get(id=request.POST.get('kelas'))
			for k in krs:
				if asd.matkul == k.kelas.matkul:
					return redirect('tampilan_mahasiswa')
			sks_matkul = asd.matkul.sks
			if cek_sks(sks_mhs, total_sks, sks_matkul):
				form.save()
			return redirect('tampilan_mahasiswa')
	form.fields['kelas'].queryset = kelas
	return render(request, 'mahasiswa/krs_form.html', {'form': form,
													   'sks_mhs': sks_mhs})


@user_passes_test(cek_mhs, login_url='/')
def mahasiswa_hapus_krs(request, id):
	krs = Krs.objects.get(id=id)
	# if  request.method=="POST":
	if request.user.mahasiswa == krs.mhs:
		krs.delete()
	return redirect('tampilan_mahasiswa')


@user_passes_test(cek_mhs, login_url='/')
def mahasiswa_jadwal_kuliah(request):
	periode = Periode.objects.all().order_by('id').reverse()[0]
	print(periode)

	def get_jadwal_by_hari(hari):
		return Krs.objects.filter(kelas__jam_kuliah__hari=hari, mhs=request.user.mahasiswa, periode=periode)

	template_name = 'mahasiswa/jadwal_kuliah.html'
	context = {'judul': 'Jadwal Kuliah',
			   'jadwal_hari_senin': get_jadwal_by_hari(2),
			   'jadwal_hari_selasa': get_jadwal_by_hari(3),
			   'jadwal_hari_rabu': get_jadwal_by_hari(4),
			   'jadwal_hari_kamis': get_jadwal_by_hari(5),
			   'jadwal_hari_jumat': get_jadwal_by_hari(6),
			   'jadwal_hari_sabtu': get_jadwal_by_hari(7),

			   }
	return render(request, template_name, context)
