from django.urls import path, include
from .views import login, logout, home, tampilan_admin, tampilan_mahasiswa, tampilan_dosen, dosen_manajemen_kelas, \
    dosen_manajemen_matkul, dosen_jadwal_mengajar, dosen_daftar_mahasiswa, dosen_tambah_matkul, dosen_edit_matkul, \
    dosen_kelola_matkul, dosen_hapus_matkul, dosen_tambah_kelas, dosen_edit_kelas, dosen_hapus_kelas, \
    mahasiswa_tambah_krs, mahasiswa_hapus_krs, mahasiswa_jadwal_kuliah, reset_matkul, dosen_get_detail_kelas, \
    dosen_manajemen_dosen, dosen_tambah_dosen, dosen_kelola_dosen, dosen_edit_dosen

urlpatterns = [

    path('home/', home, name='home_page'),
    path('', login, name='login_page'),
    path('logout/', logout, name='logout_page'),

    
    path('mahasiswa/', include([
        path('krs/',include([
            path('',tampilan_mahasiswa, name='tampilan_mahasiswa'),
            path('tambah', mahasiswa_tambah_krs, name='mahasiswa_tambah_krs'),
            path('delete/<int:id>/', mahasiswa_hapus_krs, name='mahasiswa_hapus_krs')
        ])),

        path('jadwal',mahasiswa_jadwal_kuliah, name='mahasiswa_jadwal_kuliah')

    ])),



    path('dosen/',include([
        # path('manajemen-kelas', dosen_manajemen_kelas, name='dosen_manajemen_kelas'),
        # path('', tampilan_dosen, name='tampilan_dosen'),
        path('manajemen-kelas/', include([
            path('', dosen_manajemen_kelas, name='dosen_manajemen_kelas'),
            # path('kelola', dosen_kelola_kelas, name='dosen_kelola_kelas'),
            path('tambah', dosen_tambah_kelas, name="dosen_tambah_kelas"),
            path('edit/<int:id>/', dosen_edit_kelas, name="dosen_edit_kelas"),
            path('delete/<int:id>/', dosen_hapus_kelas, name="dosen_hapus_kelas"),
            ])),
        path('manajemen-matkul/',include([
            path('', dosen_manajemen_matkul, name='dosen_manajemen_matkul'),
            path('kelola', dosen_kelola_matkul, name='dosen_kelola_matkul'),
            path('tambah', dosen_tambah_matkul, name="dosen_tambah_matkul"),
            path('reset', reset_matkul, name="reset_matkul"),
            path('edit/<int:id>/', dosen_edit_matkul, name="dosen_edit_matkul"),
            path('delete/<int:id>/', dosen_hapus_matkul, name="dosen_hapus_matkul"),
            ])),
        path('manajemen-dosen/',include([
            path('',dosen_manajemen_dosen, name='dosen_manajemen_dosen'),
            path('kelola', dosen_kelola_dosen, name='dosen_kelola_dosen'),
            path('tambah',dosen_tambah_dosen, name='dosen_tambah_dosen'),
            path('edit/<int:id>/',dosen_edit_dosen, name='dosen_edit_dosen'),


        ])),

        path('jadwal-mengajar', dosen_jadwal_mengajar, name='dosen_jadwal_mengajar'),
        path('jadwal-mengajar/detail/<int:id>/', dosen_get_detail_kelas, name="dosen_get_detail_kelas"),
        path('daftar-mahasiswa', dosen_daftar_mahasiswa, name='dosen_daftar_mahasiswa')


        # path('tambah', tambah_dosen, name="tambah_dosen"),
        # path('edit/<int:id>/', edit_dosen, name="edit_dosen"),
        # path('delete/<int:id>/', hapus_dosen, name="hapus_dosen"),
    ]))

    # path('dosen/',include(
    #         path()
    #     ))

    # path('aktor/', include('aktor.urls')),
    # path('jadwal/', include('jadwal.urls')),
    # path('krs/', include('krs.urls')),
    # path('matkul/', include('matkul.urls')),
    # path('prodi/', include('prodi.urls')),
    # path('',include('show.urls')),
]
