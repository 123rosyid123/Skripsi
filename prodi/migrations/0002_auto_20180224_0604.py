# Generated by Django 2.0.2 on 2018-02-24 06:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prodi', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gedung',
            name='kampus',
            field=models.PositiveSmallIntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)], default=1),
        ),
        migrations.AlterUniqueTogether(
            name='fakultas',
            unique_together={('kode', 'nama')},
        ),
    ]
