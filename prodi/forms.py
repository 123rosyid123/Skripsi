from django import forms
from .models import Ruang

class RuangForm(forms.ModelForm):
    class Meta:
        model = Ruang
        fields = ['gedung', 'kode', 'nama', 'lt', 'kapasitas', 'is_kelas', 'is_aktif']