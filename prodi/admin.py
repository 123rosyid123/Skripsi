from django.contrib import admin
from .models import Fakultas,Gedung,Prodi,Ruang
# Register your models here.



class AdminFakultas(admin.ModelAdmin):
    list_display = ('kode', 'nama')
class AdminGedung(admin.ModelAdmin):
    list_display = ('kode', 'nama', 'kampus')
class AdminProdi(admin.ModelAdmin):
    list_display = ('kode', 'nama', 'fakultas')
class AdminRuang(admin.ModelAdmin):
    list_display = ('kode', 'nama', 'kapasitas')

admin.site.register(Fakultas, AdminFakultas)
admin.site.register(Gedung, AdminGedung)
admin.site.register(Prodi, AdminProdi)
admin.site.register(Ruang, AdminRuang)
