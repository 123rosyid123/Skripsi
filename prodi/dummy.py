
import random
from djipsum.faker import FakerModel

def random_fakultas():
	lst = ['Kesehatan', 'FKI', 'FAI', 'Kedokteran', 'Teknik', 'Hukum']
	return random.choice(lst)


def fakultas_faker(maximum=10):
	"""
	./manage.py djipsum -auto -cg=prodi.dummy.fakultas_faker --max=2
	"""
	faker = FakerModel(app='prodi', model='Fakultas')
	object_list = []

	for _ in range(maximum):
		fields = {
			'kode': faker.fake.ean(length=8),
			'nama': random_fakultas(),
			#'author': faker.fake_relations(type='fk', field_name='author')
		}
		instance = faker.create(fields)
		object_list.append(instance)
	return object_list