from django.urls import path, include
from .views import *

urlpatterns = [
    path('ruang/',include([
        path('', home, name="home_ruang_view"),
        path('tambah/', tambah_ruang, name="tambah_ruang"),
        path('edit/<int:id>/', edit_ruang, name="edit_ruang"),

    ]))
    # path('', home, name="home_ruang_view"),
    # path('tambah/', tambah_ruang, name="tambah_ruang"),
    # path('edit/<int:id>/', edit_ruang, name="edit_ruang"),
    # path('delete/<int:id>/', hapus_ruang, name="hapus_ruang"),
]

