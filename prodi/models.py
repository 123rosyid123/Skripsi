from django.db import models


KAMPUS = (
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
)

LANTAI = (
    (0, 'Basement/Ground'),
    (1, 'Satu'),
    (2, 'Dua'),
    (3, 'Tiga'),
    (4, 'Empat'),
    (5, 'Lima'),
)

class Fakultas(models.Model):
    kode = models.CharField(max_length=10)
    nama = models.CharField(max_length=256)

    class Meta:
        verbose_name_plural = "Fakultas"
        verbose_name = "Fakultas"
        unique_together = ('kode', 'nama')

    def __str__(self):
        return self.nama

class Prodi(models.Model):
    fakultas = models.ForeignKey(Fakultas, on_delete=models.CASCADE)
    kode = models.CharField(max_length=10)
    nama = models.CharField(max_length=256)

    class Meta:
        verbose_name_plural = "Prodi"
        verbose_name = "Prodi"

    def __str__(self):
        return "{}" .format(self.nama)

class Gedung(models.Model):
    kampus = models.PositiveSmallIntegerField(choices=KAMPUS, default=1)
    kode = models.CharField(max_length=10)
    nama = models.CharField(max_length=110)

    def __str__(self):
        return '{}' .format(self.nama)
    class Meta:
        verbose_name_plural = "Gedung"
        verbose_name = "Gedung"

class Ruang(models.Model):
    gedung = models.ForeignKey(Gedung, on_delete=models.CASCADE)
    kode = models.CharField(max_length=10)
    nama = models.CharField(max_length=110)
    lt = models.PositiveSmallIntegerField(choices=LANTAI, default=0)
    kapasitas = models.PositiveSmallIntegerField()
    is_kelas = models.BooleanField(default=True)  # jika True maka bisa dipakai utk jadwal kuliah
    is_aktif = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = "Ruang"
        verbose_name = "Ruang"

    def __str__(self):
        return '{}.{}' .format(self.gedung, self.kode)