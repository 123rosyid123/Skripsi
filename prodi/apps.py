from django.apps import AppConfig


class ProdiConfig(AppConfig):
    name = 'prodi'
