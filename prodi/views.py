from django.shortcuts import render, redirect
from .forms import RuangForm
# Create your views here.
from .models import Ruang

def home(request):
    ruang = Ruang.objects.all()
    return render(request, 'ruang.html', {'ruang' : ruang})

def tambah_ruang(request):
    form = RuangForm(request.POST or None)

    if form.is_valid():
        print(request.POST.getlist('kode'))
        form.save()
        return redirect('home_ruang_view')

    return render(request, 'ruang_form.html', {'form':form})

def edit_ruang(request, id):
    ruang = Ruang.objects.get(id = id)
    form = RuangForm(request.POST or None, instance = ruang)

    if form.is_valid():
        form.save()
        return redirect('home_ruang_view')

    return render(request, 'ruang_form.html', {'form':form, 'ruang':ruang})

def hapus_ruang(request,id):
    ruang = Ruang.objects.get(id=id)

    if  request.method=="POST":
        ruang.delete()
        return redirect('home_ruang_view')

    return render(request, 'aktor_delete.html', {'ruang' : ruang})