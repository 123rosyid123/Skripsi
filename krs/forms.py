from django import forms
from .models import Krs

class KrsForm(forms.ModelForm):
    class Meta:
        model = Krs
        fields = ['kelas']