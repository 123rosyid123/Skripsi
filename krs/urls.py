from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name="home_krs_view"),
    path('tambah/', tambah_krs, name="tambah_krs"),
    path('edit/<int:id>/', edit_krs, name="edit_krs"),
    path('delete/<int:id>/', hapus_krs, name="hapus_krs"),
]

