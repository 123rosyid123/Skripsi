from django.db import models

# Create your models here.
from jadwal.models import Kelas
from aktor.models import Mahasiswa
from prodi.models import Ruang
from jadwal.models import Periode

class Krs(models.Model):
    periode = models.ForeignKey(Periode, on_delete=models.CASCADE)
    kelas = models.ForeignKey(Kelas, on_delete=models.CASCADE)
    mhs =  models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    # ruang = models.ForeignKey(Ruang, on_delete=models.CASCADE)
    waktu = models.DateTimeField(auto_now=True)
    waktu_buat = models.DateTimeField(auto_now_add=True,auto_created=True)

    class Meta:
        verbose_name_plural = "KRS"
        verbose_name = "KRS"
        unique_together = ('kelas','periode')
    def __str__(self):
        return '{} {} {}' .format(self.periode, self.kelas, self.mhs)

class Sks_ips (models.Model):
    ips_minimum = models.FloatField()
    sks = models.PositiveSmallIntegerField()

    def __str__(self):
        return '{} : {}' .format(self.ips_minimum, self.sks)