# Generated by Django 2.0.2 on 2018-05-18 05:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jadwal', '0016_auto_20180517_1208'),
        ('krs', '0006_auto_20180517_1208'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='krs',
            unique_together={('kelas', 'periode')},
        ),
    ]
