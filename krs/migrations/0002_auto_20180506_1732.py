# Generated by Django 2.0.2 on 2018-05-06 17:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('krs', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='krs',
            options={'verbose_name': 'KRS', 'verbose_name_plural': 'KRS'},
        ),
    ]
