from django.shortcuts import render, redirect
from .forms import KrsForm
# Create your views here.
from .models import Krs

def home(request):
    krs = Krs.objects.all()
    return render(request, 'krs.html', {'krs' : krs})

def tambah_krs(request):
    form = KrsForm(request.POST or None)

    if form.is_valid():
        print(request.POST.getlist('kode'))
        form.save()
        return redirect('home_krs_view')

    return render(request, 'krs_form.html', {'form':form})

def edit_krs(request, id):
    krs = Krs.objects.get(id = id)
    form = KrsForm(request.POST or None, instance = krs)

    if form.is_valid():
        form.save()
        return redirect('home_krs_view')

    return render(request, 'krs_form.html', {'form':form, 'krs':krs})

def hapus_krs(request,id):
    krs = Krs.objects.get(id=id)

    if  request.method=="POST":
        krs.delete()
        return redirect('home_krs_view')

    return render(request, 'aktor_delete.html', {'krs' : krs})