from django.db import models
from matkul.models import Matkul
from aktor.models import Dosen
from prodi.models import Ruang

SEMESTER = (
    (1, 'Ganjil'),
    (2, 'Genap'),
)

URUTAN = (
    ('A', 'A'),
    ('B', 'B'),
    ('C', 'C'),
    ('D', 'D')
)

HARI = (
    (2, 'Senin'),
    (3, 'Selasa'),
    (4, 'Rabu'),
    (5, 'Kamis'),
    (6, 'Jumat'),
    (7, 'Sabtu'),
)

SLOT_WAKTU = (
    (1, '07.00 - 07.50'),
    (2, '07.50 - 08.40'),
    (3, '08.40 - 09.30'),
    (4, '09.30 - 10.20'),
    (5, '10.20 - 11.10'),
    (6, '11.10 - 12.00'),
    (7, '12.30 - 13.20'),
    (8, '13.20 - 14.10'),
    (9, '14.10 - 15.00'),
    (10, '15.20 - 16.10'),
    (11, '16.10 - 17.00'),
    (12, '17.00 - 17.50'),
    (13, '18.20 - 19.10'),
    (14, '19.10 - 20.00'),
    (15, '20.00 - 20.50'),
    (15, '20.50 - 21.40'),

)


class Periode(models.Model):
    kode = models.CharField(max_length=10)
    tahun = models.PositiveIntegerField()
    semester = models.PositiveSmallIntegerField(choices=SEMESTER)

    def __str__(self):
        return '{}{}'.format(self.tahun, self.semester)

    class Meta:
        verbose_name_plural = "Periode"
        verbose_name = "Periode"

class JamKuliah(models.Model):
    hari = models.PositiveSmallIntegerField(choices=HARI)
    slot = models.PositiveSmallIntegerField(choices=SLOT_WAKTU)

    class Meta:
        verbose_name_plural = "Jam Kuliah"
        verbose_name = "Jam Kuliah"
        unique_together = ("hari", "slot")

    def __str__(self):
        return '{} : {}'.format(self.get_hari_display(), self.get_slot_display())

class JamDosen(models.Model):
    periode =models.ForeignKey(Periode, on_delete=models.CASCADE)
    dosen = models.ForeignKey(Dosen, on_delete=models.CASCADE)
    jam_kuliah = models.ManyToManyField(JamKuliah)

    class Meta:
        unique_together = ('periode','dosen')

class Kelas(models.Model):
    matkul = models.ForeignKey(Matkul, on_delete=models.CASCADE)
    periode = models.ForeignKey(Periode, on_delete=models.CASCADE)
    dosen = models.ManyToManyField(Dosen)
    ruang = models.ForeignKey(Ruang, on_delete=models.CASCADE, null=True)
    jam_kuliah = models.ForeignKey(JamKuliah, on_delete=models.CASCADE)
    nama = models.CharField(max_length=2,choices=URUTAN, default='A')  # A, B, C dst...
	# is_penuh = models.BooleanField(default=False)
    kapasitas = models.PositiveSmallIntegerField(default=40)
    # jam = jam()

    def jam(self):
        jam = set()
        hari_kamus={
            1 : 'Senin',
            2 : 'Selasa',
            3 : 'Rabu',
            4 : 'Kamis',
            5 : 'Jumat',
            6 : 'Sabtu'
        }
        # print(self.matkul)
        for i in range(self.matkul.sks):
            # print(i)
            jam.add(hari_kamus[self.jam_kuliah.hari-1]+str(self.jam_kuliah.slot+i))
        # print('aw')
        return jam


    class Meta:
        verbose_name_plural = "Kelas"
        verbose_name = "Kelas"
        unique_together = (('matkul', 'periode', 'nama'),
						   ('periode','ruang','jam_kuliah'))


    def __str__(self):
        return '{}:{}'.format(self.matkul.nama, self.nama)
