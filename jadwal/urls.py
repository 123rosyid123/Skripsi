from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name="home_kelas_view"),
    path('tambah/', tambah_kelas, name="tambah_kelas"),
    path('edit/<int:id>/', edit_kelas, name="edit_kelas"),
    path('delete/<int:id>/', hapus_kelas, name="hapus_kelas"),
]

