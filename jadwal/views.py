from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .forms import KelasForm
# Create your views here.
from jadwal.models import Kelas
from aktor.models import Dosen
from matkul.models import Matkul

def home(request):
    kelas = Kelas.objects.all()
    return render(request, 'kelas.html', {'kelas' : kelas})

@login_required(login_url='/show/')
def tambah_kelas(request):
    try:
        if request.user.groups.filter(name='Kaprodi').exists():
            dosen = Dosen.objects.filter(prodi=request.user.dosen.prodi)
            matkul = Matkul.objects.filter(prodi=request.user.dosen.prodi)
            # mk = Matkul(prodi=prodi_dosen)
            if request.method == 'POST':
                form = KelasForm(request.POST)
                form.fields['dosen'].queryset = dosen
                form.fields['matkul'].queryset = matkul

                if form.is_valid():
                    # print(request.POST.getlist('kode'))
                    form.save()
                    return redirect('home_kelas_view')
            else:
                form = KelasForm()
                form.fields['dosen'].queryset = dosen
                form.fields['matkul'].queryset = matkul
                


            return render(request, 'kelas_form.html', {'form':form})
        else :
            return redirect('home_kelas_view')
    except Exception as eror:
        print(eror)
        return redirect('home_kelas_view')



def edit_kelas(request, id):
    kelas = Kelas.objects.get(id = id)
    form = KelasForm(request.POST or None, instance = kelas)

    if form.is_valid():
        form.save()
        return redirect('home_kelas_view')

    return render(request, 'kelas_form.html', {'form':form, 'kelas':kelas})

def hapus_kelas(request,id):
    kelas = Kelas.objects.get(id=id)

    if  request.method=="POST":
        kelas.delete()
        return redirect('home_kelas_view')

    return render(request, 'aktor_delete.html', {'kelas' : kelas})