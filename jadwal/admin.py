from django.contrib import admin

# Register your models here.
from .models import Periode, JamKuliah, Kelas, JamDosen

class AdminJamKuliah(admin.ModelAdmin):
    list_display = ("hari", 'Jam_kuliah')

    def Jam_kuliah(self, obj):
        '''asdasdasd'''
        return "{}" .format(obj.slot)
    JamKuliah.short_description = 'Jam ke'

class AdminKelas(admin.ModelAdmin):
    list_display = ("matkul", 'nama')

class AdminPeriode(admin.ModelAdmin):
    list_display = ("kode", 'tahun', "periode")
admin.site.register(Periode)
admin.site.register(JamKuliah, AdminJamKuliah)
admin.site.register(Kelas, AdminKelas)
admin.site.register(JamDosen)
