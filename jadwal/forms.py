from django import forms
from .models import Kelas, JamDosen, JamKuliah


class KelasForm(forms.ModelForm):

    class Meta:
        model = Kelas
        fields = ['matkul', 'dosen', 'nama', 'kapasitas']

    # def __init__(self, *args , **kwargs)


class KelasFormEdit(forms.ModelForm):

    class Meta:
        model = Kelas
        fields = ['matkul', 'dosen', 'nama','ruang', 'jam_kuliah', 'kapasitas']


class JamDosenForm(forms.ModelForm):
    class Meta:
        model = JamDosen
        fields = ('dosen','jam_kuliah')

