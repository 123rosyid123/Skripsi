from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ('kode', 'nama', 'sks', 'semester', 'is_aktif', 'jenis')