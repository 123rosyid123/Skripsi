from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name="home_matkul_view"),
    path('tambah/', tambah_matkul, name="tambah_matkul"),
    path('edit/<int:id>/', edit_matkul, name="edit_matkul"),
    path('delete/<int:id>/', hapus_matkul, name="hapus_matkul"),
]

