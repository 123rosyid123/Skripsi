from django.db import models
from prodi.models import Prodi
from django.db.models import IntegerField, Model
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.
JENIS_MATKUL = (
    (0, 'Praktikum'),
    (1, 'Teori')
)
SEMESTER = (
    (1, 'Satu'),
    (2, 'Dua'),
    (3, 'Tiga'),
    (4, 'Empat'),
    (5, 'Lima'),
    (6, 'Enam'),
    (7, 'Tujuh'),
    (8, 'Delapan'),
    (9, 'Pilihanganjil'),
    (10, 'Pilihangenap'),
    (11, 'GanjilGenap')
)

class Matkul(models.Model):
    kode = models.CharField(max_length=10, unique=True)
    nama = models.CharField(max_length=256)
    semester = models.PositiveIntegerField(default=9, choices = SEMESTER)
    sks = models.IntegerField(
        default=1,
        validators=[
            MaxValueValidator(10),
            MinValueValidator(1)
        ]
     )
    # sks = models.PositiveSmallIntegerField()
    is_aktif = models.BooleanField(default=True)
    prodi = models.ForeignKey(Prodi, on_delete=models.CASCADE)  # untuk MKDU nama prodi adalah Universitas
    jenis = models.PositiveSmallIntegerField(default=1, choices=JENIS_MATKUL)
    def __str__(self):
        return '{}'.format(self.nama)

    class Meta:
        verbose_name_plural = "Mata kuliah"
        verbose_name = "Mata kuliah"
