from django.contrib import admin
from .models import Matkul
# Register your models here.



class AdminMatkul(admin.ModelAdmin):
    list_display = ('kode', 'nama', 'sks', 'semester', 'prodi')

admin.site.register(Matkul, AdminMatkul)