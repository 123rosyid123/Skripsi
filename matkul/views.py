from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from .forms import MatkulForm
from prodi.models import Prodi
# Create your views here.
from aktor.models import Dosen
from .models import Matkul

def home(request):
    if request.user.is_authenticated:
        try:
            matkul = Matkul.objects.filter(prodi= request.user.dosen.prodi)

        except Dosen.DoesNotExist:
            if  request.user.mahasiswa:
                matkul = Matkul.objects.filter(prodi= request.user.mahasiswa.prodi)

    else :
        matkul = Matkul.objects.all()
    return render(request, 'matkul.html', {'matkul' : matkul})

@login_required(login_url='/show/')
def tambah_matkul(request):
    try:
        if request.user.groups.filter(name='Kaprodi').exists():
            prodi_dosen = request.user.dosen.prodi
            mk = Matkul(prodi=prodi_dosen)
            # print('asdna')
            if request.method == 'POST':
                form = MatkulForm(request.POST, instance=mk)
                if form.is_valid():
                    # print(request.POST.getlist('kode'))
                    form.save()
                    return redirect('home_matkul_view')
            else:
                form = MatkulForm(instance=mk)

            return render(request, 'matkul_form.html', {'form':form})
        else :
            return redirect('home_matkul_view')
    except Exception as eror:
        print(eror)
        return redirect('home_matkul_view')

@login_required(login_url='/show/')
def edit_matkul(request, id):
    try:
        if request.user.groups.filter(name='Kaprodi').exists():
            matkul = Matkul.objects.get(id = id)
            form = MatkulForm(request.POST or None, instance = matkul)

            if form.is_valid():
                form.save()
                return redirect('home_matkul_view')

            return render(request, 'matkul_form.html', {'form':form, 'matkul':matkul})
        else:
            return redirect('home_matkul_view')
    except Exception as eror:
        print(eror)
        return redirect('home_matkul_view')

@login_required(login_url='/show/')
def hapus_matkul(request,id):
    matkul = Matkul.objects.get(id=id)

    if  request.method=="POST":
        matkul.delete()
        return redirect('home_matkul_view')

    return render(request, 'aktor_delete.html', {'matkul' : matkul})