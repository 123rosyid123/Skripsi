# Generated by Django 2.0.2 on 2018-05-22 18:18

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('matkul', '0011_auto_20180522_1802'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matkul',
            name='sks',
            field=models.IntegerField(default=1, validators=[django.core.validators.MaxValueValidator(10), django.core.validators.MinValueValidator(1)]),
        ),
    ]
